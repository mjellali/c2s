#include "tcp.hpp"
#include <unistd.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <thread>
#include <cerrno>
#include <arpa/inet.h>

void tcp::config(Json::Value config){
    TCP_PORT = (uint16_t)config["port"].asInt();
}


void tcp::phandle_client(int func(char*, char* ), int new_socket, char *add)
{
    int valread = 1;
    char buffer[1024] = {0};

    while (true) 
    {
        valread = read(new_socket, buffer, 1024);
        if (valread > 0)
        {
            func(buffer, add);
            bzero(buffer, 1024);
        }
        else
        {
            break;
        }
    }
    close(new_socket);
    add = (char *)"0";
}

void tcp::plisten(int func(char*, char* ))
{

    int opt = 1;
    socklen_t addrlen;
    server_fd = socket(AF_INET, SOCK_STREAM, 0);
    setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;

    address.sin_port = htons(TCP_PORT);

    bind(server_fd, (struct sockaddr *)&address, sizeof(address));
    listen(server_fd, 3);
    while (true)
    {
        struct sockaddr_in client_address;

        int new_socket = accept(server_fd, (struct sockaddr *)&client_address, &addrlen);
        if (new_socket < 0)
        {
            // error
            perror("!!! accept");
            if ((errno == ENETDOWN || errno == EPROTO || errno == ENOPROTOOPT || errno == EHOSTDOWN ||
                    errno == ENONET || errno == EHOSTUNREACH || errno == EOPNOTSUPP || errno == ENETUNREACH))
            {
                continue;
            }
            exit(EXIT_FAILURE);
        }
        else
        {
            char *c_address = strdup(inet_ntoa(client_address.sin_addr));
            std::thread connection(phandle_client, func, new_socket, c_address);
            connection.detach();
        }
    }
}

int tcp::psend(const char *destination, char* msg)
{
    int sockfd;
    struct sockaddr_in remote_addr;

    /* Get the Socket file descriptor */
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        fprintf(stderr, "ERROR: Failed to obtain Socket Descriptor! (errno = %d)\n", errno);
        exit(1);
    }

    /* Fill the socket address struct */
    remote_addr.sin_family = AF_INET;
    remote_addr.sin_port = htons(TCP_PORT);
    inet_pton(AF_INET, destination, &remote_addr.sin_addr);
    bzero(&(remote_addr.sin_zero), 8);

    /* Try to connect the remote */
    if (connect(sockfd, (struct sockaddr *)&remote_addr, sizeof(struct sockaddr)) == -1)
    {
        printf("ERROR: Failed to connect to the host! (errno = %d)\n", errno);
        return 1 ;
    }

    send(sockfd, msg, 1024, 0);
    close(sockfd);
    return 0;
}
