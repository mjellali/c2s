#ifndef HTTP_H
#define HTTP_H
#include "protocol.hpp"
#include "httplib.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <string>
#include <iostream>



class http : public protocol {

	public:
        std::string HTTP_DOMAIN;
        int HTTP_PORT;


        virtual char* get_protocol_name(){ return (char*)"HTTP";}


        virtual void config(Json::Value config);

		/**
		 * Listen for incoming connection requests
		 */
		virtual void plisten(int func(char* , char* ));

		/**
		 * Sends a message to te defined destination if it exists on this server
		 */
		virtual int psend(const char* destination, char* msg);
};

#endif
