#ifndef TCP_H
#define TCP_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <string>
#include <iostream>
#include "protocol.hpp"



class tcp : public protocol{

	public:
		int new_socket[10]; //clients fd(socket/file_descriptor) table
		char* address_list[10]; // clients addresses corresponding to fd in new_socket
		int server_fd; // server socket/fd
		struct sockaddr_in address; // server address
		int TCP_PORT;


        virtual char* get_protocol_name(){ return (char*)"TCP";}


        virtual void config(Json::Value config);

		/**
		 * Handle connection wit one client
		 */
		static void phandle_client(int func(char* , char* ), int new_socket, char* add);

		/**
		 * Listen for incoming connection requests
		 */
		virtual void plisten(int func(char* , char* ));
		/**
		 * Sends a message to te defined destination if it exists on this server
		 */
		virtual int psend(const char* destination, char* msg);
};

#endif
