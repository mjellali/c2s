

#include <iostream>
#include <string>
#include <list>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>
#include <netinet/in.h>
#include <jsoncpp/json/json.h>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/writer.h>
#include <jsoncpp/json/value.h>
#include <thread>
#include <unistd.h>
#include "testUtil.hpp"


#include "icmpp.hpp"


using namespace CppUnit;


class TestIcmpp : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestIcmpp);
    CPPUNIT_TEST(TestGet_protocol_name);
    CPPUNIT_TEST(TestConfig);
    CPPUNIT_TEST(TestPsend);
    CPPUNIT_TEST(TestPlisten);
    CPPUNIT_TEST(TestCommunication);
    CPPUNIT_TEST_SUITE_END();


protected:
    void TestGet_protocol_name(void);
    void TestConfig(void);
    void TestPhandle_client(void);
    void TestPsend(void);
    void TestPlisten(void);
    void TestCommunication(void);


private:
    std::string protocol_name = "ICMP";
    icmpp t;
};


void TestIcmpp::TestGet_protocol_name(void)
{
    CPPUNIT_ASSERT(!protocol_name.compare(t.get_protocol_name()));
}

void TestIcmpp::TestConfig(void){
    Json::Value config, protocols, cicmpp;
    std::ifstream config_doc("./config.json", std::ifstream::binary);
    config_doc >> config;
    protocols = config["protocols"];

    cicmpp = protocols["icmp"];

    t.config(cicmpp);
    CPPUNIT_ASSERT(t.SRC_IP == "127.0.0.1");
}


void TestIcmpp::TestPsend(void)
{
    //CPPUNIT_ASSERT(decoded_string == base64_decode(encoded_string));
}

void TestIcmpp::TestPlisten(void)
{
//    CPPUNIT_ASSERT(decoded_string == base64_decode(encoded_string));
}

void TestIcmpp::TestCommunication(void){
    icmpp t;
    Json::Value config, protocols, encryptions, cicmpp, cdns;
    std::ifstream config_doc("./config.json", std::ifstream::binary);
    config_doc >> config;
    protocols = config["protocols"];

    cicmpp = protocols["icmp"];

    t.config(cicmpp);

    std::thread tc(&icmpp::plisten, t, testUtil::verify_data);
    tc.detach();
    t.psend("127.0.0.1", "abc");
    //usleep(1000);
}


CPPUNIT_TEST_SUITE_REGISTRATION( TestIcmpp );

int main(int argc, char* argv[])
{
    // informs test-listener about testresults
    CPPUNIT_NS::TestResult testresult;

    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);

    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener (&progress);

    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
    testrunner.run(testresult);

    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write ();

    // Output XML for Jenkins CPPunit plugin
    std::ofstream xmlFileOut("cppTestIcmppResults.xml");
    XmlOutputter xmlOut(&collectedresults, xmlFileOut);
    xmlOut.write();

    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
