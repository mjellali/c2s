

#include <iostream>
#include <string>
#include <list>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>
#include <netinet/in.h>
#include <jsoncpp/json/json.h>
#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/writer.h>
#include <jsoncpp/json/value.h>
#include <thread>
#include <unistd.h>
#include "testUtil.hpp"


#include "dns.hpp"

//using namespace std;

using namespace CppUnit;

//int handle_data(char *buffer, char *origin)
//{
//    CPPUNIT_ASSERT(buffer == "abc");
//    return 0;
//}


//-----------------------------------------------------------------------------

class TestDns : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestDns);
    CPPUNIT_TEST(TestGet_protocol_name);
    CPPUNIT_TEST(TestConfig);
    CPPUNIT_TEST(TestPsend);
    CPPUNIT_TEST(TestPlisten);
    CPPUNIT_TEST(TestCommunication);
    CPPUNIT_TEST_SUITE_END();


protected:
    void TestGet_protocol_name(void);
    void TestConfig(void);
    void TestPhandle_client(void);
    void TestPsend(void);
    void TestPlisten(void);
    void TestCommunication(void);


private:
    std::string protocol_name = "DNS";
    dns t;
};

//-----------------------------------------------------------------------------

void TestDns::TestGet_protocol_name(void)
{
    CPPUNIT_ASSERT(!protocol_name.compare(t.get_protocol_name()));
}

void TestDns::TestConfig(void){
    Json::Value config, protocols, cdns;
    std::ifstream config_doc("./config.json", std::ifstream::binary);
    config_doc >> config;
    protocols = config["protocols"];

    cdns = protocols["dns"];

    t.config(cdns);
    CPPUNIT_ASSERT(t.DNS_PORT == cdns["port"].asInt());
    CPPUNIT_ASSERT(t.TUNNEL == cdns["tunnel"].asString());
}


void TestDns::TestPsend(void)
{
    //CPPUNIT_ASSERT(decoded_string == base64_decode(encoded_string));
}

void TestDns::TestPlisten(void)
{
//    CPPUNIT_ASSERT(decoded_string == base64_decode(encoded_string));
}

void TestDns::TestCommunication(void){
    dns t;
    Json::Value config, protocols, encryptions, cdns;
    std::ifstream config_doc("./config.json", std::ifstream::binary);
    config_doc >> config;
    protocols = config["protocols"];

    cdns = protocols["dns"];

    t.config(cdns);

    std::thread tc(&dns::plisten, t, testUtil::verify_data);
    tc.detach();
    usleep(1000000);
    
    CPPUNIT_ASSERT(0 == t.psend("127.0.0.1", "abc"));
    usleep(1000000);
}

//-----------------------------------------------------------------------------

CPPUNIT_TEST_SUITE_REGISTRATION( TestDns );

int main(int argc, char* argv[])
{
    // informs test-listener about testresults
    CPPUNIT_NS::TestResult testresult;

    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);

    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener (&progress);

    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
    testrunner.run(testresult);

    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write ();

    // Output XML for Jenkins CPPunit plugin
    std::ofstream xmlFileOut("cppTestDnsResults.xml");
    XmlOutputter xmlOut(&collectedresults, xmlFileOut);
    xmlOut.write();

    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
