#ifndef DNS_H
#define DNS_H
#include <sys/socket.h>
#include <netinet/in.h>
#include <string>
#include <iostream>
#include "protocol.hpp"


class dns : public protocol
{
public:
    int DNS_PORT=0;
    std::string TUNNEL;


    virtual char* get_protocol_name()
    {
        return (char*)"DNS";
    }
    virtual void config(Json::Value config);
    virtual void serverInit(int *sock, struct sockaddr_in* serveraddr);
    virtual void clientInit(int *sock, struct sockaddr_in* serv, const char* destination);
    virtual int sendRequest(int sock, sockaddr_in serveraddr, char *data, int data_len);
    virtual void plisten(int func(char* , char* ));
    virtual int psend(const char* destination, char* msg);
};

#endif  
