#include "http.hpp"
#include "base64.h"

void http::config(Json::Value config){
    HTTP_DOMAIN = config["domain"].asString();
    HTTP_PORT = (uint16_t)config["port"].asInt();
}

/**
 * listens for incoming http requests and decodes the values of "c2s" header containing packet data
 */
void http::plisten(int func(char*, char* ))
{
    httplib::Server svr;
    svr.Get("/", [&](const httplib::Request &req, httplib::Response &res)
    {
        std::string decoded = base64_decode(req.get_header_value("c2s").data());
        func(const_cast<char*>(decoded.data()), (char*)req.get_header_value("REMOTE_ADDR").data());
        res.set_content("OK", "text/plain");
    });

    svr.listen(HTTP_DOMAIN.data(), HTTP_PORT);
};

/**
 * encodes the packet data and store it in "c2s" header of http request then send request to server
 */
int http::psend(const char *destination, char* msg)
{
    std::string s(msg);
    std::string encoded = base64_encode(reinterpret_cast<const unsigned char*>(msg), 1024);

    httplib::Client cli(destination, HTTP_PORT);

    httplib::Headers headers =
    {
        { "c2s", encoded }
    };

    auto res = cli.Get("/", headers);
    if (res && res->status != 200)
    {
        std::cout << res->body << std::endl;
    }
    return 0;
};
