#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <jsoncpp/json/value.h>


class protocol {
	public:
	virtual char* get_protocol_name(){ return (char*)"protocol";};
	virtual void config(Json::Value config){};
	virtual void plisten(int func(char* , char* )){};
    virtual int psend(const char* destination, char* msg){return 0;};
};

#endif
