#include <iostream>
#include <string>
#include <list>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>
#include <netinet/in.h>

#include "base64.h"


using namespace CppUnit;


class TestBase64 : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestBase64);
    CPPUNIT_TEST(testEncode);
    CPPUNIT_TEST(testDecode);
    CPPUNIT_TEST_SUITE_END();


protected:
    void testEncode(void);
    void testDecode(void);

private:
    std::string decoded_string = "qwertyuiop[]\\asdfghjkl;'zxcvbnm,./`1234567890-=+_)(*&^%$#@!~}{POIUYTREWQ\":LKJHGFDSA?><MNBVCXZ";
    std::string encoded_string = "cXdlcnR5dWlvcFtdXGFzZGZnaGprbDsnenhjdmJubSwuL2AxMjM0NTY3ODkwLT0rXykoKiZeJSQjQCF+fXtQT0lVWVRSRVdRIjpMS0pIR0ZEU0E/PjxNTkJWQ1ha";
};


void TestBase64::testEncode(void)
{
    CPPUNIT_ASSERT(encoded_string == base64_encode(reinterpret_cast<const unsigned char*>(decoded_string.data()), decoded_string.length()));
}

void
TestBase64::testDecode(void)
{
    CPPUNIT_ASSERT(decoded_string == base64_decode(encoded_string));
}



CPPUNIT_TEST_SUITE_REGISTRATION( TestBase64 );

int main(int argc, char* argv[])
{
    // informs test-listener about testresults
    CPPUNIT_NS::TestResult testresult;

    // register listener for collecting the test-results
    CPPUNIT_NS::TestResultCollector collectedresults;
    testresult.addListener (&collectedresults);

    // register listener for per-test progress output
    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener (&progress);

    // insert test-suite at test-runner by registry
    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest (CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest ());
    testrunner.run(testresult);

    // output results in compiler-format
    CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
    compileroutputter.write ();

    // Output XML for Jenkins CPPunit plugin
    std::ofstream xmlFileOut("cppTestBase64Results.xml");
    XmlOutputter xmlOut(&collectedresults, xmlFileOut);
    xmlOut.write();

    // return 0 if tests were successful
    return collectedresults.wasSuccessful() ? 0 : 1;
}
