CC = g++

SRCS := $(wildcard *.c)
BINS := $(SRCS:%.c=%)

all: ${BINS}

c2s : c2s.cpp helper.hpp base64.cpp tcp.cpp http.cpp dns.cpp icmpp.cpp icmp.h icmpp.hpp tcp.hpp http.hpp httplib.h dns.hpp protocol.hpp dnsutils.h base64.h encryption/AES.cpp encryption/AES.h encryption/encryption.h
	${CC} c2s.cpp base64.cpp tcp.cpp http.cpp dns.cpp icmpp.cpp encryption/AES.cpp -o c2s -lpthread -ljsoncpp

TestBase64 : TestBase64.cpp
	${CC} base64.cpp TestBase64.cpp -o TestBase64.o -lcppunit 

TestTcp : TestTcp.cpp
	${CC} tcp.cpp TestTcp.cpp testUtil.cpp -o TestTcp.o -lcppunit -ljsoncpp -lpthread

TestHttp : TestHttp.cpp
	${CC} http.cpp TestHttp.cpp testUtil.cpp base64.cpp -o TestHttp.o -lcppunit -ljsoncpp -lpthread

TestDns : TestDns.cpp
	${CC} dns.cpp TestDns.cpp testUtil.cpp base64.cpp -o TestDns.o -lcppunit -ljsoncpp -lpthread -g

TestIcmp : TestIcmp.cpp
	${CC} icmpp.cpp TestIcmp.cpp testUtil.cpp base64.cpp -o TestIcmp.o -lcppunit -ljsoncpp -lpthread

clean:
	@echo "Cleaning up..."
	rm c2s
