#ifndef HELPER_H
#define HELPER_H
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <iostream>

int data_type(char *buffer)
{
    std::string data(buffer);
    if (!strcmp(data.substr(0, 15).data(), "|?|FILE_SYNC|?|"))
    {
        return 1; // strat receiving a file request
    }
    else if (!strcmp(data.substr(0, 15).data(), "|?|FILE_DATA|?|"))
    {
        return 2; // file data packet
    }
    else if (!strcmp(data.substr(0, 15).data(), "|?|FILE_DONE|?|"))
    {
        return 3; // file end confirmation
    }
    else
    {
        return 4; // simple message/command
    }
    return 0;
}

std::string get_file_name(char *buffer)
{
    std::string name(buffer);
    name = name.substr(15);
    name = name.substr(0, name.find_first_of('|', 0));
    return name;
}


int get_data_position(char *buffer)
{
    std::string data(buffer);
    std::string pos = data.substr(16);
    pos = pos.substr(pos.find_first_of('|') + 3);
    pos = pos.substr(0, pos.find_first_of('|'));
    return std::stoi(pos);
}



#endif