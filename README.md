C2S is a project that provides a tool for security researchers to test IDS systems.

The goal is to design an extensible tool that allows pentesters to easily test their implementation/configuration of IDS/DIP/Firewall system against common vulnerabilities such as tunneling and encryption.

C2S key feature is that arbitrary files can be remotely loaded from or to the CCS and securing undetectable and encrypted communication between the clients and the C2 server.

# Communication
    - TCP
    - HTTP
    - DNS
    - ICMP
# ENCRYPTION
    - AES

# future
    at the current state the tool only provide encrypted file and msg transfer, this can be easilty upgraded to run command remotely 

# ENV setup - linux
## required tools
    - G++
    - Make
    - libjsoncpp-dev (apt package)

## Verification
    - Run the following command on terminal from project root : chmod +x devEnvCheck.sh && ./devEnvCheck.sh

## Build c2s
    - run command "make c2s" from project root 
## manual testing
    - communication with localhost
    - communication between two instances on the same network
    - communication between remote clients

## AWS
ssh -i "c2s.pem" ubuntu@ec2-3-94-57-157.compute-1.amazonaws.com

## jenkins
http://ec2-3-94-57-157.compute-1.amazonaws.com:8081

# bugs
     - simultanious multifile handling
     - better handling off data/packet size
     - code refactoring and better modularisation 