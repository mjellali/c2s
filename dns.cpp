#include <vector>
#include <sstream>
#include <fstream>
#include <streambuf>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>
#include "dnsutils.h"		/* misc. dns related functionality */
#include "base64.h"			/* for encoding/decoding */
#include <errno.h>
#include <signal.h>
#include "dns.hpp"
#include "base64.h"



#define BUFSIZE 65536
#define MAX_DNS_PKT_SIZE 576
#define URL_SIZE 1024
#define MAXSEG 63
#define TXT_SEG_LEN 200
#define CHECKSEG 40


struct sigaction action;					/* for timeout */


enum state
{
    recving,
    sending,
    s_reset
};

enum status
{
    start,
    cont,
    endz,
    invalid,
    st_reset
};

/* global variables */
static enum state mode;

using namespace std;

int getencURLPart(unsigned char *ureq, unsigned char *uenc_urlp, enum status* flag, std::string tunnel)
{
    char *req = (char *) ureq;
    char *enc_urlp = (char *) uenc_urlp;
    char url[URL_SIZE];
    char wrapped_urlp[MAXSEG + 1];

    if(flag == NULL)
    {
        fprintf(stderr, "NULL POINTER");
        exit(1);
    }
    enum status stat = *flag;

    /* skipping header */
    int offset = sizeof(struct dns_header);

    /* copy url from req to url[] */
    strcpy(url, req + offset);

    int wr_len = url[0];
    strncpy(wrapped_urlp, req + offset + 1, wr_len);

    wrapped_urlp[wr_len] = '\0';

    if(wrapped_urlp[wr_len - tunnel.find_first_of(".") - 1] == 'c')
        stat = cont;
    else if(wrapped_urlp[wr_len - tunnel.find_first_of(".") - 1] == 'e')
        stat = endz;
    else
        stat = invalid;


    string temp(wrapped_urlp);

    strcpy(enc_urlp, temp.substr(1, wr_len - tunnel.find_first_of(".") - 2).data());
    *flag = stat;
    return strlen(enc_urlp);
}

int getTXTresponse(unsigned char *ureq, unsigned char *ures, char *txt)
{

    char *req = (char *) ureq;
    char *res = (char *) ures;

    /* Handling the header */
    struct dns_header *res_dns;
    memcpy(res, req, sizeof(struct dns_header));
    res_dns = (struct dns_header *) res;
    res_dns->ans_count = htons(1);
    res_dns->add_count = htons(0);
    res_dns->authans = 1;
    res_dns->queryresp = 1;

    int offset = sizeof(struct dns_header);	/* calculating offset for future use */
    unsigned short name_pos = 49152 | (offset);	/* position of query name for dns (1100 0000 0000 0000 | position)*/

    /* copying the query name string */
    strcpy(res + offset, req + offset);
    offset += strlen(req + offset) + 1;	/* recomputing offset with query string added */
    memcpy(res + offset, req + offset, sizeof(struct question));	/* copying type and class */

    struct question *info = (struct question *) (req + offset);
    offset += sizeof(struct question);

    /* begin answer in response */
    struct answer_rr *answer = (struct answer_rr *) (res + offset);
    answer->ptr = htons(name_pos);
    answer->atype = info->qtype;
    answer->aclass = info->qclass;
    answer->ttl_msb = htons(0);
    answer->ttl_lsb = htons(0);
    answer->len = htons(1 + strlen(txt));

    offset += sizeof(answer_rr);

    *(res + offset) = strlen(txt);
    offset++;

    /* putting text into the response */
    memcpy(res + offset, txt, strlen(txt));
    offset += strlen(txt);
    return offset;
}

void timerinterrupt(int sig)
{
    printf("timeout");
}

void dns::serverInit(int *sock, struct sockaddr_in* serveraddr)
{

    if((*sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
    {
        perror("Socket creation failed");
        exit(1);
    }

    /* Fill serveraddr with port number and address */
    serveraddr->sin_family = AF_INET;
    serveraddr->sin_port = htons(DNS_PORT);
    serveraddr->sin_addr.s_addr=INADDR_ANY;	/* listen on all interfaces */
    memset(serveraddr->sin_zero, '\0', sizeof serveraddr->sin_zero);

    /* Bind socket */
    if(bind(*sock, (struct sockaddr*) serveraddr, sizeof(struct sockaddr_in)) < 0)
    {
        perror("Bind:");
        exit(1);
    }


    return;

}

int recvRequest(int sock, unsigned char *data, int func(char*, char* ), std::string tunnel)
{

    /* declaring and initializing to 0*/
    unsigned char req[MAX_DNS_PKT_SIZE];	/* request buffer */
    unsigned char res[MAX_DNS_PKT_SIZE];	/* response buffer */
    memset(req, 0, MAX_DNS_PKT_SIZE);
    memset(res, 0, MAX_DNS_PKT_SIZE);

    /* rb: received bytes. sb: sent bytes */
    int rb, sb;


    /* for storing client information */
    struct sockaddr_in clientaddr;
    socklen_t len = sizeof(clientaddr);

    /* indicator enums */
    enum status stat = start;

    /* misc variables for temporary use */
    unsigned char url_part[URL_SIZE];	// stores part of url
    	string url_accumulator("");			// string for URL accumulation

    char filler_txt[] = "txt response";



    /* receiving URL in loop */
    while(true)
    {

        /* Receive request from client */
        if((rb = recvfrom(sock, req, BUFSIZE, 0, (struct sockaddr*)&clientaddr, &len)) < 0)
        {
            perror("Error in receive\n");
        }
        else
        {
            char* clienta = inet_ntoa(clientaddr.sin_addr);

            getencURLPart(req, url_part, &stat, tunnel);		/* extract url part from request */


            url_accumulator.append((char *)url_part);				/* append urlpart to accumulator */	// exp



            /* get filler response */
            int res_len = getTXTresponse(req, res, filler_txt);

            /* send response to client*/
           if((sb = sendto(sock, res, res_len, 0, (struct sockaddr*)&clientaddr, len)) < 0)
            {
                perror("Error in send:");
                //exit(1);
            }

            /* Checking if more packets expected */
            if(stat == endz)
            {
                std::string decoded = base64_decode(url_accumulator.data());
                char url_acc[1024];
                strncpy(url_acc, decoded.data(), 1024);
                func(const_cast<char*>(decoded.data()), clienta);
                mode = sending;
                url_accumulator.clear();
            }
            else if(stat == invalid)
            {
                mode = s_reset;
            }

        }
    }
    return 0;
}


int getTXTrequest(char *url, char *req, int seq)
{


    char *queryname;
    struct dns_header *dns;
    struct question *info;


    dns = (struct dns_header*) req;
    setdnsheader(dns);												// Fill buffer with DNS Header
    dns->iden = (unsigned short) htons(seq);

    queryname = (char *) (req + sizeof(struct dns_header));			// Add queryname to  buffer
    dnsname(queryname, url);										// Get host in DNS format

    int tot = sizeof(struct dns_header) + strlen(queryname) + 1;	// 1 zero byte after queryname

    int type_txt = 16;
    info = (struct question*) (req + tot);						// Add question type and class to buffer
    info->qtype = htons(type_txt);
    info->qclass = htons(1);
    tot += sizeof(struct question);

    return tot;
}


void dns::clientInit(int *sock, struct sockaddr_in* serv, const char* destination)
{

    /* creating socket */
    if((*sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
    {
        perror("Socket creation failed");
        exit(1);
    }

    /* getting server for sending */
    serv->sin_family = AF_INET;
    serv->sin_port = htons(DNS_PORT);						// Set port no. 53 which is reserved for DNS
    serv->sin_addr.s_addr = inet_addr(destination);

    /* setting up action*/
    sigemptyset(&action.sa_mask);
    action.sa_handler = timerinterrupt;

    if (sigaction(SIGALRM, &action, NULL) == -1)
    {
        perror("sigaction");
        exit(1);
    }

}


int dns::sendRequest(int sock, sockaddr_in serveraddr, char *data, int data_len)
{

    char req[BUFSIZE];
    char res[BUFSIZE];

    vector <string> split_data;
    std::string encoded(data);// = base64_encode(reinterpret_cast<const unsigned char*>(data), 1024);

    string data_str(encoded.data());
    for(int i = 0; i < data_len; i += CHECKSEG)
    {
        string tmp = data_str.substr(i, CHECKSEG);
        string str_temp = "c";
        str_temp.append(tmp.data());
        str_temp.append("c");
        split_data.push_back(str_temp);
    }

    int sz = split_data.size();
    int len = split_data[sz-1].length();
    split_data[0][0] = 's';
    split_data[sz-1][len-1] = 'e';

    char data_char[URL_SIZE];



    int sb, rb;
    socklen_t servlen = sizeof(serveraddr);

    for(int i = 0; i < sz; ++i)
    {
         
        split_data[i].append(TUNNEL);

        strcpy(data_char, split_data[i].c_str());


        int req_size = getTXTrequest(data_char, req, i);

        /* send response */
label:
        alarm(5);
        if((sb = sendto(sock, req, req_size, 0, (struct sockaddr*)&serveraddr, servlen)) < 0)
        {
            perror("Send failed");
            exit(1);
        }
        /* wait for response */
        if((rb = recvfrom(sock, res, BUFSIZE, 0, (struct sockaddr*)&serveraddr, &servlen)) < 0)
        {
            if(errno == EINTR)
            {
                goto label;
            }
            perror("Error in receive\n");
            exit(1);
        }


        /* ignore dummy responses */
    }

    alarm(0);

    return 0;

}


void dns::config(Json::Value config){
    DNS_PORT = (uint16_t)config["port"].asInt();
    TUNNEL = config["tunnel"].asString();
}



void dns::plisten(int func(char*, char* ))
{
    int udpsock;					/* socket server listens on */
    struct sockaddr_in serveraddr;	/* addr info for server */
    unsigned char data[1024];				/* for storing request URLS. url size is max 1024 chars */

    /* Initializing the server
     *  - get socket
     *  - fill in serveraddr (port : 53, interface : ANY_ADDR
     */
    serverInit(&udpsock, &serveraddr);

    /* the server runs within a loop */

    mode = recving;
    /* listen for requests */
    recvRequest(udpsock, data, func, TUNNEL);



    bzero(data, 1024);
};


int dns::psend(const char* destination, char* msg)
{
    int udpsock;
    struct sockaddr_in serveraddr;


    /* initialize client */
    clientInit(&udpsock, &serveraddr, destination);

    /* get input from user */
    std::string encoded = base64_encode(reinterpret_cast<const unsigned char*>(msg), 1024);
    char holder[encoded.length()];
    strcpy(holder, encoded.data());

    /* send request for URL */
    sendRequest(udpsock, serveraddr, holder, encoded.length());
    encoded.clear();

    return 0;
};


