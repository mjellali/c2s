#ifndef _ENCRYPTION_H_
#define _ENCRYPTION_H_

#include <string>
#include "AES.h"

AES aes(128);
unsigned char iv[] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

void encrypt(std::string enc, char **msg)
{
    unsigned int len;
    unsigned char* encrypted = aes.EncryptCFB((unsigned char *)*msg, 1024, (unsigned char *)"c2s", iv, len);
    *msg = (char*) encrypted;
}

void decrypt(std::string enc, char **msg)
{
    unsigned char* decrypted = aes.DecryptCFB( (unsigned char*)*msg, 1024, (unsigned char *)"c2s", iv);
    *msg = (char*)decrypted;
}

#endif