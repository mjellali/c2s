#include "icmpp.hpp"
#include "icmp.h"
#include "base64.h"			/* for encoding/decoding */




void icmpp::config(Json::Value config)
{
    SRC_IP = config["hostip"].asString();
}


void icmpp::plisten(int func(char*, char* ))
{
    struct icmp_packet packet;
    int sock_fd;

    sock_fd = open_icmp_socket();
    bind_icmp_socket(sock_fd);

    while(1)
    {
        receive_icmp_packet(sock_fd, &packet);
        std::string decoded = base64_decode(packet.payload);
        func(const_cast<char*>(decoded.data()), packet.src_addr);
        bzero(packet.payload, packet.payload_size);

    }

    close_icmp_socket(sock_fd);
}


int icmpp::psend(const char *destination, char* msg)
{
    struct icmp_packet packet;
    char *src_ip = (char *) malloc(100);;
    int sock_fd;
    strcpy(src_ip, SRC_IP.data());

    strncpy(packet.src_addr, src_ip, strlen(src_ip) + 1);
    strncpy(packet.dest_addr, (char*)destination, strlen(destination) + 1);
    set_reply_type(&packet);
    std::string encoded = base64_encode(reinterpret_cast<const unsigned char*>(msg), 1024);
    packet.payload = (char*)malloc(encoded.size()+10);
    strcpy(packet.payload, encoded.data());
    packet.payload_size = strlen(packet.payload);

    sock_fd = open_icmp_socket();

    send_icmp_packet(sock_fd, &packet);

    close_icmp_socket(sock_fd);
    return 0;
}
