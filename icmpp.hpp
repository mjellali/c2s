#ifndef ICMPP_H
#define ICMPP_H
#include "protocol.hpp"
#include <sys/socket.h>
#include <netinet/in.h>
#include <string>
#include <iostream>



class icmpp : public protocol
{

public:
    std::string SRC_IP;


    virtual char* get_protocol_name()
    {
        return (char*)"ICMP";
    }


    virtual void config(Json::Value config);

    /**
     * Listen for incoming connection requests
     */
    virtual void plisten(int func(char*, char* ));

    /**
     * Sends a message to te defined destination if it exists on this server
     */
    virtual int psend(const char* destination, char* msg);
};

#endif

